import request from '../utils/request.js'
import state from '../store/index.js'

export function getLottery(){
  return request({
      url:`/lottery/detail/${state.getters.getLotteryId}`,
      method:'get'
  })
}

export function getHistory(page = 1,size = 10){
  return request({
     url:`/lottery/issue-record/${state.getters.getLotteryId}?page=${page}&pageSize=${size}`,
     method: 'get'
  })
}

export function getBets(page=1,size=10){
  return request({
     url:`/lottery/bet-record/${state.getters.getLotteryId}?page=${page}&pageSize=${size}`,
     method: 'get'
  })
}

export function doBet(data){
  return request({
    url:'/lottery/bet',
    method: 'post',
    data
  })
}
