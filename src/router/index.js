import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'index',
      component: ()=>import('@/views/index')
    },
    {
      path: '/index',
      name: 'index',
      component: ()=>import('@/views/index')
    },
    {
      path: '/405',
      name: '405',
      component: ()=>import('@/views/405')
    },
    {
      path:'/404',
      name: '/404',
      component: ()=>import('@/views/404')
    },
    {
      path:'/bet',
      name:'bet',
      component: ()=>import('@/views/bet/index')
    },
    {
      path:'/follow',
      name:'betDone',
      component: ()=>import('@/views/betDone/index')
    },
    {
      path:'/data',
      name:'data',
      component: ()=>import('@/views/list/index')
    },
    {
      path:'/data/history',
      name:'history-record',
      component: ()=>import('@/views/noticePage/history')
    },
    {
      path:'/*',
      redirect: '/404'
    }
  ]
})
