import state from '../store/index.js'

export function randomCode() {

    let type = state.getters.getLotteryType

    var result = '';
    switch (type) {
        case "六合彩":
            for (let i = 0; i < 7; i++) {
                //todo
                result +=parseInt(Math.random()*50) + ' ';
            }
            break;
        case "扑克":
            var r = ['A','2','3','4','5','6','7','8','9','T','J','Q','K']
            var k = ['s','c','d','h'];  //黑桃 梅花 方块 红桃

            var exist = [];

            for (let i = 0; i < 3; i++) {
                var random = parseInt(Math.random()*r.length*k.length-i)
                while (exist.indexOf(random)>-1){
                    random ++;
                }

                exist.push(random);

                var x = random % k.length;
                var y =parseInte(random / k.length);
                result +=r[y] + k[x] + ' ';
            }
            break;
        case "PC蛋蛋":
            for (let i = 0; i < 3; i++) {
                result +=parseInt(Math.random()*10)+' ';
            }
            break;
        case "时时彩":
            for (let i = 0; i < 5; i++) {
                result +=parseInt(Math.random()*10)+' ';
            }
            break;
        case "赛车":
            var r = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10'];
            r.sort(function (a, b) {
                return Math.random() - 0.5;
            })
            for (let i = 0; i < r.length; i++) {
                result += r[i] + ' ';
            }
            break;
        case "快三":
            for (let i = 0; i < 3; i++) {
                result += parseInt((Math.random() * 6 + 1)) + ' ';
            }
            break;
        case "11选5":
            var r = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11']
            for (let i = 0; i < 5; i++) {
                var index = parseInt(Math.random() * r.length)
                result += r[index] + ' ';
                r.splice(index, 1);
            }
            break;
    }
    return result.trim();
}
