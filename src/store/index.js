import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
	state:{
      token:localStorage.getItem("77live-token"),
      averageOdds:0.00,
      selected:[],
      lotteryId:localStorage.getItem("77live-lotteryId"),
      lotteryType:'',
      lotteryName:'',
      issue:'',
      per:0,
      games:[],
      methodName:'',
      methodId:0,
      methodList:[],
      money:0,
      killTime:new Date(),
      killCode:'222222'
	},
	getters: {
    getLotteryName:state=>state.name,
    getIssue:state=>state.issue,
    getLotteryType:state=>state.lotteryType,
    getLotteryId:state=>state.lotteryId,
    getKillCode:state=>state.killCode,
    getToken:state=>state.token,
    getAverageOdds:state=>state.averageOdds,
    getSelected:state=>state.selected,
    getPer:state=>state.per,
    getGames:state=>state.games,
    getMethods:state=>state.methodList,
    getMethodName:state=>state.methodName,
    getMethodId:state=>state.methodId,
    getMoney:state=>Number(state.money),
    getKillTime:state=>state.killTime,
    getKill:state=>{
      let dt = state.killTime
      if(!dt)
       return '00:00:00'

      var h = dt.getHours();
      var m = dt.getMinutes();
      var s = dt.getSeconds();
      if(s>57){
         s = 0
         m ++
         if(m === 60){
           m = 0
           h ++
           if(h === 24)
             h = 0
         }
      }

      if(h<10) h = '0' + h
      if(m<10) m = '0' + m
      if(s<10) s = '0' + s

      return `${h}:${m}:${s}`
    }
	},
	mutations: {
    setLotteryName(store,name){
        store.lotteryName = name
    },
    setIssue(store,issue){
        store.issue = issue
    },
    setLotteryType(store,type){
        store.lotteryType = type
    },
    setKillCode(store,code){
       store.killCode = code
    },
    setKillTime(store,time){
       store.killTime = time
    },
    setMoney(store,money){
      store.money = money
    },
    setLotteryId(store,id){
       store.lotteryId = id
       localStorage.setItem("77live-lotteryId",id)
    },
    setToken(store,token){
      store.token = token
      localStorage.setItem("77live-token",token)
    },
    setAverageOdds(store,odds){
      if(odds){
        store.averageOdds = odds
      }else{
        store.averageOdds = 0
      }
    },
    setSelected(store,selected){
      if(selected){
        store.selected = selected
      }else{
        store.selected = []
      }
    },
    setPer(store,per){
      if(per){
        store.per = per
      }else{
        store.per = 0
      }
    },
    setMethodList(store,arr){
       store.methodList = arr
       if(arr && arr.length>0){
         let flag = false
         arr.forEach(e=>{
            if(store.methodId == e.id)
              flag = true
         })
         
         if(!flag){
           store.methodId = arr[0].id
           store.games = arr[0].games
         }  
       }
    },
    setMethodName(store,name){
      store.methodName = name
    },
    setGames(store,games){
      store.games = games
    },
    setMethodId(store,id){
      store.methodId = id
    }
	},
	acitons: {

	}
});

export default store
