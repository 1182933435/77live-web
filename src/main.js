// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import store from './store';
import App from './App'
import router from './router'
import Vant from 'vant';
import 'vant/lib/index.css';
import { Notify } from 'vant'

Vue.use(VueAxios, axios)
Vue.use(Vant)
Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})


window.setLotteryId = function(id){
   store.commit("setLotteryId",id)
}

window.setMoney = function(money){
  store.commit('setMoney',money)
}

window.followBet = function(lotteryId,seleted){
    store.commit('setLotteryId',lotteryId)
    if(seleted){
      let arr = []
      if(Object.prototype.toString.call(seleted) === '[object Array]'){
          arr = seleted
      }else if(Object.prototype.toString.call(seleted)=="[object String]"){
          arr = seleted.split('&')
      }
      
      arr.sort()

      if(arr.length > 0){
        store.commit('setSelected',arr)
        router.push({
          path:'follow'
        })
      }
    }
}

window.notice = function(msg,type){
   Notify({
     type:type || 'primary',
     message:msg
   })
}
